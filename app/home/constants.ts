export const FORECAST_KEY = "8c0fcaa4e1a81e43d8aaafad2c03181f";
export const FORECAST_ROOT = "https://api.darksky.net/forecast/";
export const GOOGLE_KEY = "AIzaSyCT4LLFpzIQ0SysMQ4HS_RzA325HwKs9Xk";
export const GOOGLE_ROOT = "https://maps.googleapis.com/maps/api/geocode/json";

export const WEATHER_COLORS = {
    'default': {
         'background-image': 'url("../../assets/weather/default.jpg")',
         'background-repeat': 'no-repeat',
         'background-attachment': 'fixed',
         'background-position': 'center',
         'color': '#FFF'
    },
    'clear-day': {
        'background-image': 'url("../../assets/weather/clear-day.jpg")',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#FFF'
    },
    'clear-night': {
        'background-image': 'url("../../assets/weather/clear-night.jpg")',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#FFF'
    },
    'rain': {
        'background-image': 'url("../../assets/weather/rain.jpg")',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#FFF'
    },
    'snow': {
        'background-image': 'url("../../assets/weather/snow.jpg")',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#0288D1'
    },
    'sleet': {
        'background-image': 'url("../../assets/weather/sleet.jpg")',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#000'
    },
    'wind': {
        'background-image': 'url("../../assets/weather/wind.jpg")',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#000'
    },
    'fog': {
        'background-image': 'url("../../assets/weather/fog.jpg")',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#FFF'
    },
    'cloudy': {
        'background-image': 'url("../../assets/weather/cloudy.jpg") center fixed',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#000'
    },
    'partly-cloudy-day': {
        'background-image': 'url("../../assets/weather/partly-cloudy.jpg")',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#000'
    },
    'partly-cloudy-night': {
        'background-image': 'url("../../assets/weather/partly-cloudy-night.jpg")',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center',
        'color': '#FFF'
    }
}
