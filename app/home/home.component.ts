import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService, User } from '../users/users.service';
import { WeatherService } from './home.service';
import { Weather } from './weather';
import { WEATHER_COLORS } from './constants';

declare var Skycons: any;

@Component({
  moduleId: module.id,
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.css' ],
  providers: [ AuthenticationService, WeatherService ]
})
export class HomeComponent implements OnInit{
  pos: Position;
  weatherData = new Weather(null, null, null, null, null);
  currentSpeedUnit = "kph";
  currentTempUnit = "fahrenheit";
  currentLocation = "";
  icons = new Skycons();
  constructor(
    private _service:AuthenticationService,
    private service: WeatherService){ }
    ngOnInit(){
      this._service.checkCredentials();
      this.getCurrentLocation();
    }
    getCurrentLocation() {
      this.service.getCurrentLocation()
      .subscribe(position => {
        this.pos = position;
        this.getCurrentWeather();
        this.getLocationName();
      },
      err => console.error(err))
    }
    getCurrentWeather() {
      this.service.getCurrentWeather(this.pos.coords.latitude, this.pos.coords.longitude)
      .subscribe(weather => {
        this.weatherData.temp = weather["currently"]["temperature"],
        this.weatherData.wind = weather["currently"]["windSpeed"],
        this.weatherData.humidity = weather["currently"]["humidity"],
        this.weatherData.icon = weather["currently"]["icon"],
        this.weatherData.summary = weather["currently"]["summary"],
        console.log(weather);
        this.setIcon();

      },
      err => console.error(err));
    }
    logout() {
      this._service.logout();
    }
    getLocationName() {
      this.service.getLocationName(this.pos.coords.latitude, this.pos.coords.longitude)
          .subscribe(location => {
            console.log(location);
            this.currentLocation = location["results"][1]["formatted_address"];
            console.log(this.currentLocation);
          });
    }

    toggleTempUnits() {
      if(this.currentTempUnit == "fahrenheit") {
        this.currentTempUnit = "celsius";
      } else {
        this.currentTempUnit = "fahrenheit";
      }
    }
    toggleSpeedUnits() {
      if(this.currentSpeedUnit == "kph") {
        this.currentSpeedUnit = "mph";
      } else {
        this.currentSpeedUnit = "kph";
      }
    }
    toggleUnits() {
      this.toggleTempUnits();
      this.toggleSpeedUnits();
    }
    setIcon() {
      this.icons.add("icon", this.weatherData.icon);
      this.icons.play();
    }
    setStyles(): Object {
      if(this.weatherData.icon) {
        this.icons.color = WEATHER_COLORS[this.weatherData.icon]["color"];
        return WEATHER_COLORS[this.weatherData.icon];
      } else {
        this.icons.color = WEATHER_COLORS["default"]["color"];
        return WEATHER_COLORS["default"];
      }
    }
  }
