"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var angular_2_local_storage_1 = require("angular-2-local-storage");
var speed_unit_pipe_1 = require("../app/home/pipe/speed-unit.pipe");
var temp_unit_pipe_1 = require("../app/home/pipe/temp-unit.pipe");
var app_component_1 = require("./app.component");
var app_routes_1 = require("./app.routes");
var login_component_1 = require("./login/login.component");
var header_component_1 = require("./header/header.component");
var home_component_1 = require("./home/home.component");
var footer_component_1 = require("./footer/footer.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule,
            angular_2_local_storage_1.LocalStorageModule.withConfig({
                prefix: 'my-app',
                storageType: 'localStorage'
            }), app_routes_1.routing, forms_1.FormsModule, http_1.JsonpModule, http_1.HttpModule
        ],
        declarations: [app_component_1.AppComponent, login_component_1.LoginComponent, header_component_1.HeaderComponent, home_component_1.HomeComponent, footer_component_1.FooterComponent, speed_unit_pipe_1.SpeedUnitPipe, temp_unit_pipe_1.TempUnitPipe],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map