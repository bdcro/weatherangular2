import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { JsonpModule, HttpModule } from '@angular/http';
import { LocalStorageModule } from 'angular-2-local-storage';
import { SpeedUnitPipe } from '../app/home/pipe/speed-unit.pipe';
import { TempUnitPipe } from '../app/home/pipe/temp-unit.pipe';

import { AppComponent } from './app.component';
import { routing } from './app.routes';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
    imports: [ BrowserModule,
      LocalStorageModule.withConfig({
            prefix: 'my-app',
            storageType: 'localStorage'
        }), routing, FormsModule, JsonpModule, HttpModule
     ],
    declarations: [ AppComponent, LoginComponent, HeaderComponent,HomeComponent, FooterComponent, SpeedUnitPipe, TempUnitPipe ],
    bootstrap: [ AppComponent]
})

export class AppModule { }
