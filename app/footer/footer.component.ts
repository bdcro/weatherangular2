import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'footer',
    templateUrl: './footer.component.html',
    styleUrls: [ './footer.component.css' ]
})
export class FooterComponent {
}
