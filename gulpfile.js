var gulp = require('gulp'),
    concat = require('gulp-concat'),
    postcss = require('gulp-postcss'),
    rename = require('gulp-rename'),
    autoprefixer = require('autoprefixer'),
    lost = require('lost'),
    rucksack = require('gulp-rucksack'),
    sass = require('gulp-sass'),
    cssNano = require('cssnano'),
    processors = [autoprefixer , lost , rucksack];

var cssLocations = 'app/**/*.scss';

gulp.task('buildComponentCss', function () {
    gulp.src(cssLocations)
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(rucksack())
    // Builds each file into its source folder
    .pipe(gulp.dest(function(file) {
        return file.base;
    }))
    .pipe(postcss([
          cssNano
    ]))
    .pipe(rename(function(file) {
        return file.extname = ".css"
    }))
    .pipe(gulp.dest(function(file) {
        return file.base;
    }))
});

gulp.task('sass-main', function () {
    gulp.src('styles.scss')
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(rucksack())
    .pipe(gulp.dest('./'));
});

gulp.task('default', ['buildComponentCss','sass-main'],
function() {
    gulp.watch(cssLocations, ['buildComponentCss']);
});
