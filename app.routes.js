"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_component_1 = require("login/login.component");
var routes = [
    {
        path: 'login',
        component: login_component_1.LoginComponent,
    }
];
exports.routing = RouterModule.forRoot(routes);
//# sourceMappingURL=app.routes.js.map